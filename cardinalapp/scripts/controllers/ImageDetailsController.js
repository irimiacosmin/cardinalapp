import ContainerController from "../../cardinal/controllers/base-controllers/ContainerController.js";

export default class HomePageController extends ContainerController {
    constructor(element) {
        super(element);

        const model = {
            imageURL: '',
            commentTitle: {
                label: "Comment"
            },
            comment: {
                label: "Comment",
                name: "comment",
                required: false,
                placeholder: "Leave a comment",
                value: ''
            },
            comments: []
        };

        this.model = this.setModel(model);

        const catImage = getQueryVariable('image')

        $$.interactions.startSwarmAs('demo/agent/system', 'CommentsManager', 'getCommentsForPhoto', catImage)
            .onReturn((err, comments) => {
                if(err) {
                    console.log("avem eroare", err);
                }

                comments = JSON.parse(comments);

                comments = comments.map(c => { return {comm: c}});
                console.log('comments', comments);
                this.model.comments = comments;
            })

        document.dispatchEvent(new CustomEvent('modelReady', {
            bubbles: true,
            cancelable: true,
            composed: true
        }));


        $$.interactions.startSwarmAs("demo/agent/system", "FileManager", "readFile", catImage)
            .onReturn((err, photoContent) => {
                if (err) {
                    console.error(err);
                    return;
                }

                const ab = _base64ToArrayBuffer(photoContent)
                this.model.imageURL = arrayBufferToImageURL(ab);
            })

        this.on('submit-comment', undefined, (event) => {
            event.preventDefault();
            event.stopImmediatePropagation();

            console.log('gonna commit', this.model.comment.value);

            $$.interactions.startSwarmAs('demo/agent/system', 'CommentsManager', 'commentForPhoto', catImage, this.model.comment.value)
                .onReturn(err => {
                    if(err) {
                        console.log("avem eroare", err);
                    }

                    this.model.comments.push({value: this.model.comment.value});
                    this.model.comment.value = '';
                })

        });



        function getQueryVariable(variable) {
            const query = window.location.search.substring(1);
            const vars = query.split('&');
            for (let i = 0; i < vars.length; i++) {
                let pair = vars[i].split('=');
                if (decodeURIComponent(pair[0]) === variable) {
                    return decodeURIComponent(pair[1]);
                }
            }
            console.log('Query variable %s not found', variable);
        }


        function arrayBufferToImageURL(arrBuf) {
            const arrayBufferView = new Uint8Array(arrBuf);
            const blob = new Blob([arrayBufferView], {type: "image/jpeg"});
            const urlCreator = window.URL || window.webkitURL;
            const imageUrl = urlCreator.createObjectURL(blob);

            return imageUrl
        }

        function _base64ToArrayBuffer(base64) {
            const binary_string = window.atob(base64);
            const len = binary_string.length;
            const bytes = new Uint8Array(len);
            for (let i = 0; i < len; i++) {
                bytes[i] = binary_string.charCodeAt(i);
            }
            return bytes.buffer;
        }
    }
}