import ContainerController from "../../cardinal/controllers/base-controllers/ContainerController.js";

export default class HomePageController extends ContainerController {
    constructor(element) {
        super(element);
        console.log("A MERS ACEST CONTROLLER");
        const model = {
            isLoading: false,
            loadingFinished: false,
            isUploadPage: true,
            entities: [
                // {name: 'aaa'},
                // {name: 'bbbb'}
            ]
        }
        this.model = this.setModel(model);

        this.on("chooseFile", undefined, (data) => {
            const reader = new FileReader();

            this.model.isLoading = true;
            this.model.loadingFinished = false;

            reader.addEventListener("load",  () => {

                console.log('result', data.data[0])
                // const decoder = new TextDecoder('utf8');
                // const b64encoded = btoa(decoder.decode(reader.result));
                $$.interactions.startSwarmAs("demo/agent/system", "FileManager", "writeFile", data.data[0].name, reader.result)
                    .onReturn((err, result) => {
                        if (err) {
                            throw err;
                        }

                        this.model.isLoading = false;
                        this.model.loadingFinished = true;
                        console.log(result);
                    });
            }, false);

            console.log('reader', reader.readAsDataURL(data.data[0]));

        })

        this.on('get-photos', undefined, () => {
            console.log('getting called')
            $$.interactions.startSwarmAs("demo/agent/system", "FileManager", "readFileNames")
                .onReturn((err, photoNames) => {
                    if (err) {
                        throw err;
                    }

                    this.model.entities = [];
                    loadNextImage.call(this);

                    function loadNextImage(index = 0) {
                        if(index >= photoNames.length) {
                            return;
                        }

                        $$.interactions.startSwarmAs("demo/agent/system", "FileManager", "readFile", photoNames[index])
                            .onReturn((err, photoContent) => {
                                if(err) {
                                    console.error(err);
                                    return;
                                }

                                const ab = _base64ToArrayBuffer(photoContent)
                                this.model.entities.push({name: arrayBufferToImageURL(ab), photoName: photoNames[index]})

                                loadNextImage.call(this, index + 1);
                            })

                    }
                });
        })

        this.on('photo-details', undefined, (event) => {
            console.log('args', event.data);
            window.location.href = "http://127.0.0.1:8000/image-details?image=" + event.data
        })



        function arrayBufferToImageURL(arrBuf) {
            const arrayBufferView = new Uint8Array(arrBuf);
            const blob = new Blob([arrayBufferView], {type: "image/jpeg"});
            const urlCreator = window.URL || window.webkitURL;
            const imageUrl = urlCreator.createObjectURL(blob);

            return imageUrl
        }

        function _base64ToArrayBuffer(base64) {
            const binary_string = window.atob(base64);
            const len = binary_string.length;
            const bytes = new Uint8Array(len);
            for (let i = 0; i < len; i++) {
                bytes[i] = binary_string.charCodeAt(i);
            }
            return bytes.buffer;
        }
    }
}